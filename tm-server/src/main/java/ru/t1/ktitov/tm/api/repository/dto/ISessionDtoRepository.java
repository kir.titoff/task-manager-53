package ru.t1.ktitov.tm.api.repository.dto;

import ru.t1.ktitov.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {
}
