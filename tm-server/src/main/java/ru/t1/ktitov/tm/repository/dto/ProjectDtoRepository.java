package ru.t1.ktitov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m";
        return entityManager.createQuery(jpql, ProjectDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProjectDTO> criteriaQuery = criteriaBuilder.createQuery(ProjectDTO.class);
        Root<ProjectDTO> project = criteriaQuery.from(ProjectDTO.class);
        criteriaQuery.select(project)
                .where(criteriaBuilder.equal(project.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(project.get(Sort.getOrderByField(sort))));
        TypedQuery<ProjectDTO> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public ProjectDTO findOneById(@Nullable final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        @Nullable final List<ProjectDTO> projects = findAll();
        if (!projects.isEmpty())
            projects.forEach(entityManager::remove);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
