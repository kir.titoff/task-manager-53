package ru.t1.ktitov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.service.PropertyService;
import ru.t1.ktitov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User ADMIN3 = new User();

    @NotNull
    public final static User USER4 = new User();

    @NotNull
    public final static List<User> USER_LIST1 = Arrays.asList(USER1, USER2, ADMIN3);

    @NotNull
    public final static List<User> USER_LIST2 = Arrays.asList(USER4);

    @NotNull
    public final static List<User> FULL_LIST = Arrays.asList(USER1, USER2, ADMIN3, USER4);

    static {
        for (int i = 0; i < FULL_LIST.size(); i++) {
            @NotNull final User user = FULL_LIST.get(i);
            user.setLogin("user" + (i + 1));
            user.setEmail("user" + (i + 1) + "@gmail.com");
            user.setPasswordHash(HashUtil.salt(new PropertyService(), "password"));
        }
    }

}
