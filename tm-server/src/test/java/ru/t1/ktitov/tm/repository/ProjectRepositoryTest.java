package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.model.IProjectRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.repository.model.ProjectRepository;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;
import ru.t1.ktitov.tm.service.model.UserService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected IProjectRepository getRepository(@NotNull EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    private void compareProjects(@NotNull final Project project1, @NotNull final Project project2) {
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project1.getDescription(), project2.getDescription());
        Assert.assertEquals(project1.getStatus(), project2.getStatus());
        Assert.assertEquals(project1.getUser().getId(), project2.getUser().getId());
        Assert.assertEquals(project1.getCreated(), project2.getCreated());
    }

    private void compareProjects(
            @NotNull final List<Project> projectList1,
            @NotNull final List<Project> projectList2) {
        Assert.assertEquals(projectList1.size(), projectList2.size());
        for (int i = 0; i < projectList1.size(); i++) {
            compareProjects(projectList1.get(i), projectList2.get(i));
        }
    }

    @Before
    public void initData() {
        @NotNull final IUserService userService = new UserService(connectionService, propertyService);
        userService.add(USER1);
        userService.add(USER2);
        userService.add(ADMIN3);
    }

    @After
    public void tearDown() {
        @NotNull final IUserService userService = new UserService(connectionService, propertyService);
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN3.getId());
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_PROJECT1);
            repository.add(USER1_PROJECT2);
            repository.add(USER1_PROJECT3);
            repository.clear(USER2.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_PROJECT1);
            repository.clear(USER2.getId());
            compareProjects(USER1_PROJECT1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_PROJECT1);
            repository.add(USER1_PROJECT2);
            repository.add(USER1_PROJECT3);
            repository.add(USER2_PROJECT1);
            Assert.assertEquals(4, repository.findAll().size());
            Assert.assertEquals(3, repository.findAll(USER1.getId()).size());
            Assert.assertEquals(1, repository.findAll(USER2.getId()).size());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_PROJECT1);
            repository.add(USER1_PROJECT2);
            repository.add(USER1_PROJECT3);
            compareProjects(USER1_PROJECT1, repository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_PROJECT1.getId()));
            compareProjects(USER1_PROJECT1, repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
            Assert.assertNotNull(repository.findOneById(USER1_PROJECT1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_PROJECT1.getId()));
            Assert.assertNotNull(repository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_PROJECT1);
            repository.add(USER1_PROJECT2);
            repository.add(USER1_PROJECT3);
            repository.remove(USER1_PROJECT1);
            Assert.assertEquals(2, repository.findAll().size());
            repository.removeById(USER1_PROJECT2.getId());
            Assert.assertEquals(1, repository.findAll().size());
            compareProjects(USER1_PROJECT3, repository.findAll().get(0));
            repository.clear();
            repository.add(USER1_PROJECT1);
            repository.add(USER1_PROJECT2);
            repository.add(USER1_PROJECT3);
            Assert.assertEquals(3, repository.findAll().size());
            repository.removeById(USER2.getId(), USER1_PROJECT1.getId());
            Assert.assertEquals(3, repository.findAll().size());
            repository.removeById(USER1.getId(), USER1_PROJECT1.getId());
            repository.removeById(USER1.getId(), USER1_PROJECT2.getId());
            compareProjects(USER1_PROJECT3, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
