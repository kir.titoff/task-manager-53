package ru.t1.ktitov.tm.exception.field;

public final class IncorrectIndexException extends AbstractFieldException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect.");
    }

}
