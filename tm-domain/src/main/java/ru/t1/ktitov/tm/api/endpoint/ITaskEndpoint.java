package ru.t1.ktitov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.request.task.*;
import ru.t1.ktitov.tm.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    );

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskListResponse listTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    );

    @NotNull
    @WebMethod
    TaskClearResponse clearTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    );

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskGetByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskGetByProjectIdResponse getTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskGetByProjectIdRequest request
    );

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    );

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    );

}
