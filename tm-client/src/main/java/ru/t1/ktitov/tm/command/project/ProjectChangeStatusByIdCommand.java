package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change project status by id";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("ENTER STATUS: ");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setProjectId(id);
        request.setStatus(status);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
